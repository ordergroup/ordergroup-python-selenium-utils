import unittest

from selenium import webdriver

from .utils import SeleniumUtils


class BaseTestCase(unittest.TestCase):
    pass


class PagesOverviewTestCase(BaseTestCase):
    sites = [
        {'url': 'https://bitbucket.org/', 'name': 'bitbucket'},  # przykład
        {'url': 'https://www.mailgun.com/', 'name': 'mailgun'}  # przykład
    ]

    driver_class = webdriver.Chrome

    def test_takes_all_sites_screenshot(self):
        driver = self.driver_class()

        for site in self.sites:
            SeleniumUtils.take_screenshot(site['url'], site['name'], driver)

        driver.close()


if __name__ == '__main__':
    unittest.main()
