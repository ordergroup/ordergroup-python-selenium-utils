from setuptools import setup, find_packages
import os

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

root_dir = os.path.dirname(__file__)
if root_dir != '':
    os.chdir(root_dir)
package_dir = 'og_django_utils'


version = __import__('og_selenium_utils').__version__
readme = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

setup(
    name='og_selenium_utils',
    version=version,
    packages=find_packages(),
    author='Order Group',
    description='OG Selenium testing utils',
    long_description=readme,
    include_package_data=True,
    author_email='hello@ordergroup.pl',
    license='all rights reserved',
    url='ordergroup.pl',
    install_requires=[
        'python-slugify==1.2.4',
        'selenium==3.4.1'
    ]
)
